package com.ycventure.codingexercisesolution.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.ycventure.codingexercisesolution.model.AlbumsModel


/**
 * The class will create database with table
 * @version 1.0.0
 * @author Yogesh Chauripagar on 16-03-19.
 */
@Database(entities = arrayOf(AlbumsModel::class), version = 1)
abstract class AlbumDatabase : RoomDatabase() {


    abstract fun albumDao(): AlbumDao

    companion object {
        private var DB_INSTANCE: AlbumDatabase? = null

        @JvmStatic
        fun getDatabaseInstance(context: Context): AlbumDatabase? {
            if (DB_INSTANCE == null) {
                synchronized(AlbumDatabase::class.java) {
                    if (DB_INSTANCE == null) {

                        DB_INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AlbumDatabase::class.java, "albums_database")
                                .build()
                    }
                }
            }
            return DB_INSTANCE
        }
    }
}