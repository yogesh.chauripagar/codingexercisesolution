package com.ycventure.codingexercisesolution.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.ycventure.codingexercisesolution.model.AlbumsModel


/**
 * The class helps to create DAO
 * It will help to configure all the constant in single class
 * @version 1.0.0
 * @author Yogesh Chauripagar on 16-03-19.
 */
@Dao
interface AlbumDao {

    //Sort by title
    @Query("SELECT * FROM albums ORDER BY title ASC")
    fun sortAlbumsByTitles(): LiveData<Array<AlbumsModel>>

    @Insert
    fun insertAll( albumsModel: Array<AlbumsModel>)

    @Delete
    fun deleteAll( albumsModel: Array<AlbumsModel>)

}