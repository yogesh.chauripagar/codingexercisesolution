package com.ycventure.codingexercisesolution.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ycventure.codingexercisesolution.R
import com.ycventure.codingexercisesolution.model.AlbumsModel
import kotlinx.android.synthetic.main.album_item.view.*

/**
 * The class helps to bind data with the recycler view
 * @version 1.0.0
 * @author Yogesh Chauripagar on 16-03-19.
 */

class AlbumAdapter(var context: Context, var albums: Array<AlbumsModel>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var v = LayoutInflater.from(context).inflate(R.layout.album_item, parent, false)
        return Item(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Item).bindData(albums!!.get(position),context)
    }

    override fun getItemCount(): Int {

        try {
            return albums!!.size
        } catch (e: Exception) {
            return 0;
        }

    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(albumsModel: AlbumsModel,context: Context) {

            itemView.txtId.text =context.getString(R.string.id)+ albumsModel.id.toString()
            itemView.txtTitle.text =context.getString(R.string.title)+ albumsModel.title
            itemView.txtUserId.text = context.getString(R.string.user_id)+albumsModel.userId.toString()
        }
    }
}
