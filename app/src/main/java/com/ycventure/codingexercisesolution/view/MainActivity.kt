package com.ycventure.codingexercisesolution.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.ycventure.codingexercisesolution.R
import com.ycventure.codingexercisesolution.common.AppConstants
import com.ycventure.codingexercisesolution.model.AlbumViewModel
import com.ycventure.codingexercisesolution.model.AlbumsModel
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.indeterminateProgressDialog


/**
 * The class will fetch the album and display
 * @version 1.0.0
 * @author Yogesh Chauripagar on 16-03-19.
 */

class MainActivity : AppCompatActivity() {

    private lateinit var mAlbumViewModel: AlbumViewModel
    var TAG = this.javaClass.canonicalName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.mAlbumViewModel = ViewModelProviders.of(this).get(AlbumViewModel::class.java)

        // Create the observer which updates the UI.
        val albumObserver = Observer<Array<AlbumsModel>> { albumModelsArray ->
            // Update the UI
            rv_display_albums.adapter = AlbumAdapter(this@MainActivity, albumModelsArray)
        }
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        this.mAlbumViewModel.sortedAlbums.observe(this, albumObserver)


        getAplbums()

    }

    // function for fetching albums
    fun getAplbums() {

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)

        //show progress
        var progressDialog = indeterminateProgressDialog(message = getString(R.string.fetching_album_message))
        progressDialog.show()

        // Request a string response from the provided URL.
        val stringReq = StringRequest(Request.Method.GET, AppConstants.BASE_URL,
                Response.Listener<String> { response ->

                  //  Log.d("MAinActivity", "response:::$response")

                    val gson = Gson()
                    val albumsModelArray = gson.fromJson(response, Array<AlbumsModel>::class.java)
                    rv_display_albums.layoutManager = LinearLayoutManager(this)
                    rv_display_albums.hasFixedSize()

                    // rv_display_albums.adapter = AlbumAdapter(this@MainActivity, albumsModelArray)
                    try {
                        mAlbumViewModel?.insertAll(albumsModelArray)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val sortedAlbumsModel = mAlbumViewModel!!.sortedAlbums;
                   // Log.d(TAG, sortedAlbumsModel.toString())

                    if(progressDialog.isShowing)
                        progressDialog.dismiss()


                    rv_display_albums.adapter = AlbumAdapter(this@MainActivity, sortedAlbumsModel.value)
                },
                Response.ErrorListener {

                    if(progressDialog.isShowing)
                        progressDialog.dismiss()


                    Toast.makeText(this, getString(R.string.err_fetching_album_failed),
                            Toast.LENGTH_LONG).show()

                })
        queue.add(stringReq)

    }

}







