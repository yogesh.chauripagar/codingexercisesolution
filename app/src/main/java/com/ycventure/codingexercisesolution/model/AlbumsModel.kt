package com.ycventure.codingexercisesolution.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

/**
 * The class helps for accessing data like id,title as per Album
 * @version 1.0.0
 * @author Yogesh Chauripagar on 16-03-19.
 */
@Entity(tableName = "albums")
data class AlbumsModel(
        @SerializedName("id")
        @PrimaryKey
        @ColumnInfo(name="id")
        var id: Int = 0,

        @NonNull //Considering title is not null
        @SerializedName("title")
        @ColumnInfo(name="title")
        var title: String = "",

        @SerializedName("userId")
        @NonNull
        @ColumnInfo(name="userId")
        var userId: Int = 0)