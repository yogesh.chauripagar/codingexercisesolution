package com.ycventure.codingexercisesolution.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.ycventure.codingexercisesolution.data.AlbumDao
import com.ycventure.codingexercisesolution.data.AlbumDatabase
import org.jetbrains.anko.doAsync


/**
 * @author Yogesh Chauripagar on 16-03-19.
 * @version 1.0.0
 */

class AlbumViewModel(application: Application) : AndroidViewModel(application) {
    var TAG = this.javaClass.canonicalName

    private val mAlbumDao: AlbumDao
    private val mAlbumDatabase: AlbumDatabase?


    init {

        this.mAlbumDatabase = AlbumDatabase.getDatabaseInstance(application)
        this.mAlbumDao = this.mAlbumDatabase!!.albumDao()
    }

    override fun onCleared() {
        super.onCleared()
    }


    var sortedAlbums: LiveData<Array<AlbumsModel>> = mAlbumDao.sortAlbumsByTitles();

     fun insertAll(albumsModelArray: Array<AlbumsModel>) {
         doAsync {
             mAlbumDao.deleteAll(albumsModelArray)
             mAlbumDao.insertAll(albumsModelArray)
         }
    }
}
