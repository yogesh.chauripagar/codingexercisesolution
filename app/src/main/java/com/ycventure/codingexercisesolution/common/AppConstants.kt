package com.ycventure.codingexercisesolution.common

/**
 * The class helps to initialize  the variable which will be constant
 * It will help to configure all the constant in single class
 * @version 1.0.0
 * @author Yogesh Chauripagar on 16-03-19.
 */

object AppConstants {
    val BASE_URL = "https://jsonplaceholder.typicode.com/albums"
}
